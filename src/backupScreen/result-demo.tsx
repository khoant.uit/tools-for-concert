import React, {useEffect, useState} from 'react';
import {View, Text, Platform, PermissionsAndroid} from 'react-native';
import {Button, Overlay} from 'react-native-elements';
// require the module (follow instruction from Docs)
// var RNFS = require('react-native-fs');
import RNFS from 'react-native-fs';
import {ScrollView} from 'react-native-gesture-handler';

export default function HomeScreen({navigation, route}: any) {
    const {csvDataRoute, resultRoute} = route.params;
    const [input, setInput] = useState('');
    const [result, setResult] = useState([]);

    const [csvData, setCsvData] = useState('');
    const [visible, setVisible] = useState(false);
    const requestReadStoragePermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    title: 'Request external storage permission',
                    message:
                        'We need to access device storage ' +
                        'to read csv file.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            const grantedWrite = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Request external storage permission',
                    message:
                        'We need to access device storage ' +
                        'to read csv file.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );

            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the storage');
                toggleOverlay();
                setCsvData('');
                // readFileInDownload();
            } else {
                console.log('Read storage permission deny');
            }
        } catch (err) {
            console.warn(err);
        }
    };
    const toggleOverlay = () => {
        setVisible(!visible);
    };

    const openFileChosen = () => {
        requestReadStoragePermission();
    };

    // get csv data (not test in iOS yet)
    const readFileInDownload = () => {
        // get a list of files and directories in the main bundle
        RNFS.readDir(
            Platform.OS === 'ios'
                ? RNFS.MainBundlePath
                : RNFS.ExternalDirectoryPath,
        ) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
            .then((result) => {
                console.log('it ra thi no toi dc day');
                console.log('document ' + RNFS.DocumentDirectoryPath);
                // console.log('GOT RESULT', result);

                // stat the first file
                return Promise.all([RNFS.stat(result[0].path), result[0].path]);
            })
            .then((statResult) => {
                console.log('hoac la no toi duoc day');

                if (statResult[0].isFile()) {
                    // if we have a file, read it
                    return RNFS.readFile(statResult[1], 'ascii');
                }

                return 'no file on ' + Platform.OS;
            })
            .then((contents) => {
                console.log('hoac toi duoc day luon');

                // log the file contents
                console.log('found a file on ' + Platform.OS);

                // console.log(contents);
                handleCSVData(contents);
            })
            .catch((err) => {
                console.log('hoac khong toi duoc cho nao ca');
                console.log('external ' + RNFS.ExternalDirectoryPath);
                console.log('download ' + RNFS.DownloadDirectoryPath);

                console.log(err.message, err.code);
            });
    };

    const handleCalcRank = (rankingResult, teamList, judgeList) => {
        let majorNumber = judgeList.length / 2;
        let currentRankingTable = rankingResult;
        let finalRankingResultIndex: number[] = [];

        const getMajorTeamList = (currentRankingTable) => {
            let countTopList: {team: string; countVote: number}[] = [];
            for (let judgeI = 0; judgeI < judgeList.length; judgeI++) {
                for (let rankI = 0; rankI < teamList.length; rankI++) {
                    // loop through ranking result from each judge
                    // choose top result from each judge
                    // console.log(currentRankingTable[rankI][judgeI]);
                    if (currentRankingTable[rankI][judgeI] !== '') {
                        // add current result to countTopList
                        let index = countTopList.findIndex(
                            (topResult) =>
                                topResult.team ===
                                currentRankingTable[rankI][judgeI],
                        );
                        if (index === -1) {
                            // push current result to count top list
                            countTopList.push({
                                team: currentRankingTable[rankI][judgeI],
                                countVote: 1,
                            });
                        } else {
                            // increase count for current result in count top list
                            countTopList[index].countVote += 1;
                        }
                        // break the loop for current judge
                        break;
                    }
                }
            }
            // sort countTopList base on countVote
            countTopList.sort((a, b) => b.countVote - a.countVote);
            // sum countVote until countVote > G/2
            let sumCountVote = 0;
            let majorTeamList: any = [];
            for (let i = 0; i < countTopList.length; i++) {
                sumCountVote += countTopList[i].countVote;
                majorTeamList.push(countTopList[i]);
                if (sumCountVote > majorNumber) break;
            }
            // log the current majorTeamLead
            console.log('current raw majorTeamList: ', majorTeamList);

            // check if any teams has the same countVote with majorTeamList and add them
            let lastMajorTeam = majorTeamList[majorTeamList.length - 1];
            countTopList.forEach((currentTopTeam) => {
                if (currentTopTeam.countVote === lastMajorTeam.countVote) {
                    // check if currentTopTeam doesn't include in majorTeamList yet
                    if (
                        !majorTeamList.some(
                            (currentMajorTeam) =>
                                currentMajorTeam.team === currentTopTeam.team,
                        )
                    ) {
                        majorTeamList.push(currentTopTeam);
                    }
                }
            });
            console.log(
                'add any countTopList that have the same countVote with majorTeamList: ',
                majorTeamList,
            );

            return majorTeamList;
        };

        const handleCompeteTeam = (
            currentTeamCompeteList,
            currentRankingTable,
        ) => {
            let result: {
                team: string;
                countWin: number;
                winPoint: number;
                countVote: number;
            }[];
            result = currentTeamCompeteList.map((teamObj) => ({
                team: teamObj.team,
                countVote: teamObj.countVote,
                countWin: 0,
                winPoint: 0,
            }));
            let listLength = currentTeamCompeteList.length;
            // reformat currentRankingTable for easy ranking
            let reformatRankingTable: string[][] = new Array(
                judgeList.length,
            ).fill('');
            reformatRankingTable.forEach((item, index) => {
                reformatRankingTable[index] = new Array(teamList.length).fill(
                    '',
                );
            });
            for (let judgeI = 0; judgeI < judgeList.length; judgeI++) {
                for (let rankI = 0; rankI < teamList.length; rankI++) {
                    reformatRankingTable[judgeI][rankI] =
                        currentRankingTable[rankI][judgeI];
                }
            }

            // let's compete
            for (let i = 0; i < listLength - 1; i++) {
                for (let j = i + 1; j < listLength; j++) {
                    let teamIRank;
                    let teamJRank;
                    let winPointObj = {teamI: 0, teamJ: 0};
                    reformatRankingTable.forEach((judgeResult) => {
                        teamIRank = judgeResult.indexOf(
                            currentTeamCompeteList[i].team,
                        );
                        teamJRank = judgeResult.indexOf(
                            currentTeamCompeteList[j].team,
                        );
                        if (teamIRank < teamJRank) winPointObj.teamI++;
                        else if (teamIRank > teamJRank) winPointObj.teamJ++;
                        else throw 'something went wrong';
                    });
                    console.log(winPointObj);
                    console.log(
                        currentTeamCompeteList[i].team,
                        currentTeamCompeteList[j].team,
                    );
                    if (winPointObj.teamI > winPointObj.teamJ)
                        result[i].countWin++;
                    else if (winPointObj.teamI < winPointObj.teamJ)
                        result[j].countWin++;
                    else throw 'something went wrong again';
                    // add winPoint
                    result[i].winPoint += winPointObj.teamI;
                    result[j].winPoint += winPointObj.teamJ;
                    // console.log(
                    //   currentTeamCompeteList[i].team,
                    //   teamIRank < teamJRank
                    //     ? 'win'
                    //     : teamIRank > teamJRank
                    //     ? 'lose'
                    //     : 'equal',
                    //   currentTeamCompeteList[j].team,
                    // );
                }
            }

            // pick list of teams with the most countWin
            result.sort((a, b) => b.countWin - a.countWin);
            let countTop = 0;
            result.forEach((item) => {
                item.countWin === result[0].countWin && countTop++;
            });
            console.log({countTop});
            console.table(result);
            if (countTop === 1) {
                return [{team: result[0].team, countVote: result[0].countVote}];
            } else {
                let tmp = result
                    .slice(0, countTop)
                    .sort((a, b) => b.winPoint - a.winPoint);
                return [{team: tmp[0].team, countVote: tmp[0].countVote}];
            }
        };

        const handleDetermineTopTeam = (majorTeamList, currentRankingTable) => {
            // split majorTeamList into list of teams that has similar countVote to compete
            // process to compete team in list from bottom up
            // winner of team in bottom will be add to the above list

            // split teamList into list of similar countVote team to compete
            // code du thua, vi da sort 1 lan o tren
            majorTeamList.sort((a, b) => b.countVote - a.countVote);

            let teamCompeteList: {team: string; countVote: number}[] = [];
            do {
                teamCompeteList.push(majorTeamList.pop());
                if (majorTeamList.length === 0) {
                    teamCompeteList = handleCompeteTeam(
                        teamCompeteList,
                        currentRankingTable,
                    );
                } else if (
                    majorTeamList[majorTeamList.length - 1].countVote !==
                    teamCompeteList[teamCompeteList.length - 1].countVote
                ) {
                    teamCompeteList = handleCompeteTeam(
                        teamCompeteList,
                        currentRankingTable,
                    );
                }
            } while (majorTeamList.length !== 0);
            console.log({majorTeamList});
            console.log({teamCompeteList});

            return teamList.indexOf(teamCompeteList[0].team);
        };

        const getTopTeam = (currentRankingTable) => {
            let majorTeamList = getMajorTeamList(currentRankingTable);
            let topteam = handleDetermineTopTeam(
                majorTeamList,
                currentRankingTable,
            );
            return topteam;
        };

        const removeTopTeamFromRankingTable = (rankingTable, topTeamIndex) => {
            for (let rankI = 0; rankI < teamList.length; rankI++) {
                for (let judgeI = 0; judgeI < judgeList.length; judgeI++) {
                    if (
                        rankingTable[rankI][judgeI] === teamList[topTeamIndex]
                    ) {
                        rankingTable[rankI][judgeI] = '';
                    }
                }
            }
            return rankingTable;
        };

        // main program
        do {
            let topTeamIndex = getTopTeam(currentRankingTable);
            console.log({topTeamIndex});
            currentRankingTable = removeTopTeamFromRankingTable(
                currentRankingTable,
                topTeamIndex,
            );
            console.table(currentRankingTable);
            finalRankingResultIndex.push(topTeamIndex);
        } while (finalRankingResultIndex.length !== teamList.length);
        finalRankingResultIndex.forEach((index) =>
            console.log(teamList[index]),
        );
    };

    const handleCSVData = (csvContent) => {
        setCsvData(csvContent);
        if (csvContent !== '') {
            let rawData = csvContent.split(/\r\n/g);
            let teamList: string[];
            let judgeList: string[];
            teamList = rawData.slice(1, rawData.length).map((item, index) => {
                return item.trim().split(',')[0];
            });

            judgeList = rawData[0].trim().split(',');
            // remove the 'participant name'
            judgeList.shift();

            // console.log(rawData);
            let raw2DArray = new Array(teamList.length).fill('');
            raw2DArray.forEach((_item, index) => {
                raw2DArray[index] = rawData[index + 1].trim().split(',');
                raw2DArray[index].shift();
            });

            let rankingResult = new Array(teamList.length).fill('');
            rankingResult.forEach((item, index) => {
                rankingResult[index] = new Array(judgeList.length).fill('');
            });
            // console.log({rankingResult});
            // console.log({raw2DArray});
            // console.log({teamList});
            // console.log({judgeList});

            // make a ranking table with given data
            for (let judgeI = 0; judgeI < judgeList.length; judgeI++) {
                for (let teamI = 0; teamI < teamList.length; teamI++) {
                    let rankI = raw2DArray[teamI][judgeI];
                    rankingResult[Number(rankI) - 1][judgeI] = teamList[teamI];
                }
            }
            handleCalcRank(rankingResult, teamList, judgeList);
        }
    };

    useEffect(() => {
        console.log({csvDataRoute});
        setInput(csvDataRoute);
        setResult(resultRoute);
    }, [csvDataRoute, resultRoute]);

    return (
        <ScrollView>
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    // alignItems: 'center',
                }}>
                <Text
                    style={{
                        fontSize: 17,
                        textAlign: 'center',
                        fontWeight: 'bold',
                    }}>
                    INPUT CSV
                </Text>
                <Text style={{fontSize: 17}}>{input}</Text>
                <Text> </Text>
                <Text
                    style={{
                        fontSize: 17,
                        textAlign: 'center',
                        fontWeight: 'bold',
                    }}>
                    RANKING RESULT
                </Text>
                <View>
                    {result.length !== 0 &&
                        result.map((team, index) => (
                            <Text
                                key={index}
                                style={{
                                    fontSize: 17,
                                }}>{`Rank ${index + 1}: ${team}`}</Text>
                        ))}
                </View>
                <Text></Text>
                {/* <Text style={{fontSize: 17}}>{result}</Text> */}
                <Button
                    title="Go back to Home Screen"
                    onPress={() => navigation.navigate('home')}
                />
                <Text>{csvData}</Text>
            </View>
        </ScrollView>
    );
}
