import React from 'react';
import {ScrollView, View, Text} from 'react-native';
import {Button} from 'react-native-elements';

export default function CreateEventScreen({navigation}: any) {
  return (
    <ScrollView>
      <View style={{flex: 1, alignItems: 'stretch', justifyContent: 'center'}}>
        <View>
          {/* <Button
              title="Go to home page"
              style={{paddingBottom: '10px'}}
              onPress={() => navigation.navigate('home')}
            />
            <Button
              title="Go to Details again"
              onPress={() => navigation.push('details')}
            /> */}

          <View
            style={{
              height: 100,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'center', fontSize: 17}}>
              Create Event Page
            </Text>
          </View>
          <Button
            title="Go to Details page"
            onPress={() => navigation.navigate('details')}
          />
          <Button
            title="Go back to Home Screen"
            onPress={() => navigation.popToTop()}
          />
          {/* <Button
              title="Push home screen"
              onPress={() => navigation.push('event')}
            /> */}
        </View>
        <View>
          <Text style={{fontSize: 17}}>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
            volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
            ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
            consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate
            velit esse molestie consequat, vel illum dolore eu feugiat nulla
            facilisis at vero eros et accumsan et iusto odio dignissim qui
            blandit praesent luptatum zzril delenit augue duis dolore te feugait
            nulla facilisi. Nam liber tempor cum soluta nobis eleifend option
            congue nihil imperdiet doming id quod mazim placerat facer possim
            assum. Nam liber tempor cum soluta nobis eleifend option congue
            nihil imperdiet doming id quod mazim placerat facer possim assum.
          </Text>
        </View>
      </View>
    </ScrollView>
  );
}
