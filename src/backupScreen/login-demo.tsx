import React from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, Input} from 'react-native-elements';

export default function ({navigation}: any) {
    return (
        <View
            style={{
                // flex: 1,
                // justifyContent: 'center',
                top: '20%',
            }}>
            <View
                style={{width: '80%', marginLeft: 'auto', marginRight: 'auto'}}>
                <Input
                    placeholder="Username"
                    autoCompleteType="username"
                    autoFocus
                    leftIcon={<Icon name="user" size={24} color="black" />}
                />
                <Input
                    placeholder="Password"
                    secureTextEntry={true}
                    autoCompleteType="off"
                    leftIcon={
                        <Icon name="keyboard-o" size={24} color="black" />
                    }
                />
                <Button
                    onPress={() => {
                        navigation.replace('home');
                    }}
                    title="Login"
                    style={{marginBottom: 5}}
                />
            </View>
        </View>
    );
}
