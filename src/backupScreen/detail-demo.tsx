import React, {useState} from 'react';
import {ScrollView, View, Text} from 'react-native';
import {Button, Overlay} from 'react-native-elements';

export default function DetailsScreen({navigation}: any) {
    const [visible, setVisible] = useState(false);

    const toggleOverlay = () => {
        setVisible(!visible);
    };

    return (
        <ScrollView>
            <View>
                <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
                    <Text>hello overlay</Text>
                </Overlay>
            </View>
            <View
                style={{
                    flex: 1,
                    alignItems: 'stretch',
                    justifyContent: 'center',
                }}>
                <View>
                    <Button
                        title="Create New Event"
                        onPress={() => navigation.navigate('event')}
                    />
                    <Button
                        title="Go back to home page"
                        onPress={() => navigation.navigate('home')}
                    />
                    <Button title="toggle overlay" onPress={toggleOverlay} />
                    {/* <Button
              title="Go to Details again"
              onPress={() => navigation.push('details')}
            />
            <Button
              title="Go back to first screen in stack"
              onPress={() => navigation.popToTop()}
            />
  
            <Button
              title="Create New Event"
              onPress={() => navigation.navigate('event')}
            /> */}
                </View>
                <View>
                    <Text style={{fontSize: 17}}>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                        dolore magna aliquam erat volutpat. Ut wisi enim ad
                        minim veniam, quis nostrud exerci tation ullamcorper
                        suscipit lobortis nisl ut aliquifirst screen in stackp
                        ex ea commodo consequat. Duis autem vel eum iriure dolor
                        in hendrerit in vulputate velit esse molestie consequat,
                        vel illum dolore eu feugiat nulla facilisis at vero eros
                        et accumsan et iusto odio dignissim qui blandit praesent
                        luptatum zzril delenit augue duis dolore te feugait
                        nulla facilisi. Nam liber tempor cum soluta nobis
                        eleifend option congue nihil imperdiet doming id quod
                        mazim placerat facer possim assum. Nam liber tempor cum
                        soluta nobis eleifend option congue nihil imperdiet
                        doming id quod mazim placerat facer possim assum.
                    </Text>
                    <Text style={{fontSize: 17}}>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                        dolore magna aliquam erat volutpat. Ut wisi enim ad
                        minim veniam, quis nostrud exerci tation ullamcorper
                        suscipit lobortis nisl ut aliquip ex ea commodo
                        consequat. Duis autem vel eum iriure dolor in hendrerit
                        in vulputate velit esse molestie consequat, vel illum
                        dolore eu feugiat nulla facilisis at vero eros et
                        accumsan et iusto odio dignissim qui blandit praesent
                        luptatum zzril delenit augue duis dolore te feugait
                        nulla facilisi. Nam liber tempor cum soluta nobis
                        eleifend option congue nihil imperdiet doming id quod
                        mazim placerat facer possim assum. Nam liber tempor cum
                        soluta nobis eleifend option congue nihil imperdiet
                        doming id quod mazim placerat facer possim assum.
                    </Text>
                    <Text style={{fontSize: 17}}>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                        dolore magna aliquam erat volutpat. Ut wisi enim ad
                        minim veniam, quis nostrud exerci tation ullamcorper
                        suscipit lobortis nisl ut aliquip ex ea commodo
                        consequat. Duis autem vel eum iriure dolor in hendrerit
                        in vulputate velit esse molestie consequat, vel illum
                        dolore eu feugiat nulla facilisis at vero eros et
                        accumsan et iusto odio dignissim qui blandit praesent
                        luptatum zzril delenit augue duis dolore te feugait
                        nulla facilisi. Nam liber tempor cum soluta nobis
                        eleifend option congue nihil imperdiet doming id quod
                        mazim placerat facer possim assum. Nam liber tempor cum
                        soluta nobis eleifend option congue nihil imperdiet
                        doming id quod mazim placerat facer possim assum.
                    </Text>
                </View>
            </View>
        </ScrollView>
    );
}
