import {gql} from '@apollo/client';

gql`
    mutation registerUser($input: RegisterUserInput!) {
        register(input: $input) {
            id
            email
            name
            role
            companyName
            access_token
            expiresTime
        }
    }
`;
