import React from 'react';
import {Text, View, ScrollView, Pressable} from 'react-native';
import {TickIcon} from '../components/AllSvgIcons/AllSvgIcons';
import ConcertForm from '../components/ConcertForm/ConcertForm';
import Header from '../components/Header/Header';
import {theme} from '../style/theme';

export default function ConcertRegistration() {
    return (
        <>
            <Header
                title='Concert Registration'
                headerRight={
                    <Pressable style={{paddingHorizontal: 10}}>
                        <TickIcon />
                    </Pressable>
                }
            />
            <ScrollView
                style={{
                    flex: 1,
                    paddingHorizontal: 20,
                    backgroundColor: '#FFFFFF',
                }}
            >
                <ConcertForm />
                <View
                    style={{
                        flex: 1,
                        backgroundColor: theme.color.lightBackground,
                        paddingHorizontal: 33,
                        marginTop: 15,
                        marginBottom: 15,
                        borderRadius: 5,
                        paddingTop: 8,
                        paddingBottom: 8,
                    }}
                >
                    <Text
                        style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            color: theme.color.lightText,
                        }}
                    >
                        List of performance
                    </Text>
                </View>
            </ScrollView>
        </>
    );
}
