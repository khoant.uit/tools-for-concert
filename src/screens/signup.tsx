import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Header from '../components/Header/Header';
import SignupForm from '../components/SignupForm/SignupForm';

const SignupHeader = () => (
    <Header
        centerComponent={
            <View>
                <Text style={styles.textWelcome}>Sign Up</Text>
                <Text style={styles.textSubHeader}>
                    Create an account to continue
                </Text>
            </View>
        }
    />
);

export default function Signup() {
    return (
        <>
            <SignupHeader />
            <View style={styles.container}>
                <View style={styles.contentContainer}>
                    <SignupForm />
                </View>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    textWelcome: {
        fontSize: 18,
        color: 'white',
        lineHeight: 28,
        fontWeight: '700',
        textAlign: 'center',
    },
    textSubHeader: {
        fontSize: 12,
        color: 'white',
        fontWeight: '700',
        textAlign: 'center',
    },
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 30,
    },
});
