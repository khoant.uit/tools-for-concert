import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import ConcertCard from '../components/ConcertCard/ConcertCard';
import {Input} from 'react-native-elements';
import {SearchIcon} from '../components/AllSvgIcons/AllSvgIcons';
import {theme} from '../style/theme';
import {useNavigation} from '@react-navigation/core';
import FilterOptions from '../containers/FilterOptions/FilterOptions';
import Header from '../components/Header/Header';
import ConcertRegistrationButton from '../components/ConcertRegistrationButton/ConcertRegistrationButton';

export default function Home() {
    const navigation = useNavigation();
    return (
        <>
            <Header
                title='List of Concerts'
                headerLeft={<></>}
                headerRight={<ConcertRegistrationButton />}
            />
            <ScrollView>
                <View style={{marginHorizontal: 15, marginTop: 15}}>
                    <Input
                        placeholder='Search'
                        leftIcon={<SearchIcon />}
                        inputContainerStyle={{
                            borderWidth: 1,
                            borderColor: theme.color.primary,
                            borderRadius: 5,
                        }}
                        containerStyle={{
                            paddingLeft: 0,
                            paddingRight: 0,
                        }}
                    />
                </View>
                <FilterOptions
                    style={{
                        marginBottom: 9,
                        marginLeft: 15,
                        marginRight: 15,
                    }}
                />
                <ConcertCard
                    onPress={() => navigation.navigate('judge-result')}
                />
                <ConcertCard />
                <ConcertCard />
                <ConcertCard />
                <ConcertCard />
                <ConcertCard />
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({});
