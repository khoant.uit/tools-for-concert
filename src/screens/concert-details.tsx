import {useNavigation} from '@react-navigation/core';
import React, {useState} from 'react';
import {Pressable, ScrollView, View} from 'react-native';
import {
    HeartInCircle,
    HeartInCircleInvert,
} from '../components/AllSvgIcons/AllSvgIcons';
import Button from '../components/Button/Button';
// import CommentInputBox from '../components/CommentInputBox/CommentInputBox';
import ConcertInfoCard from '../components/ConcertInfoCard/ConcertInfoCard';
import Header from '../components/Header/Header';
import HeaderBackButton from '../components/HeaderBackButton/HeaderBackButton';
import PhotoSession from '../components/PhotoSession/PhotoSession';
import ResultCard from '../components/ResultCard/ResultCard';
import CommentSession from '../containers/CommentSession/CommentSession';

const ConcertDetailHeader = () => {
    const [favorite, setFavorite] = useState(false);
    return (
        <Header
            title='Concert Detail'
            headerRight={
                <Pressable
                    style={{paddingLeft: 10, paddingRight: 10}}
                    onPress={() => setFavorite(!favorite)}
                >
                    {favorite ? <HeartInCircle /> : <HeartInCircleInvert />}
                </Pressable>
            }
            headerLeft={<HeaderBackButton />}
        />
    );
};

export default function ConcertDetail() {
    const navigation = useNavigation();

    return (
        <>
            <ConcertDetailHeader />
            <View style={{flex: 1}}>
                <ConcertInfoCard
                    footer={
                        <View style={{alignItems: 'center'}}>
                            <Button
                                focus
                                title='Are you a Judge?'
                                onPress={() =>
                                    navigation.navigate('judge-login')
                                }
                            />
                        </View>
                    }
                />
                <ScrollView
                    contentContainerStyle={{
                        paddingHorizontal: 16,
                        paddingTop: 16,
                        paddingBottom: 16,
                        alignItems: 'center',
                    }}
                    style={{
                        flex: 10,
                    }}
                >
                    <ResultCard />
                    <PhotoSession />
                    <CommentSession />
                    {/* <CommentInputBox /> */}
                </ScrollView>
            </View>
        </>
    );
}
