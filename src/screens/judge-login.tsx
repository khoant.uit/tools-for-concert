import React, {useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import Input from '../components/Input/Input';
import {
    ButtonLogin,
    TextButton,
    TextError,
} from '../components/LoginForm/LoginForm.styled';
import ConcertInfoCard from '../components/ConcertInfoCard/ConcertInfoCard';
import {theme} from '../style/theme';
import {useNavigation} from '@react-navigation/core';
import Header from '../components/Header/Header';

export default function JudgeLogin() {
    const navigation = useNavigation();
    const [password, setPassword] = useState('');
    const [errMessage, setErrMessage] = useState('');

    return (
        <>
            <Header title='Judgement' />
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF',
                }}
            >
                <ConcertInfoCard />
                <ScrollView
                    contentContainerStyle={{
                        paddingHorizontal: 10,
                        alignItems: 'center',
                        paddingTop: 15,
                    }}
                    style={{
                        flex: 3,
                    }}
                >
                    <Input label='Your full name' placeholder='Your name' />
                    <Input
                        label='Password'
                        placeholder='Password'
                        value={password}
                        onChangeText={setPassword}
                    />
                    <Text
                        style={{
                            fontSize: 16,
                            color: theme.color.lightText,
                            lineHeight: 28,
                        }}
                    >
                        Please contact admin for a password
                    </Text>
                    <ButtonLogin
                        onPress={() => {
                            if (password.toLowerCase() == 'concertcode') {
                                setErrMessage('');
                                navigation.navigate('judge-result');
                            } else {
                                setErrMessage('Authentication fails.');
                            }
                        }}
                    >
                        <TextButton textColor='#FFFFFF'>Go!</TextButton>
                    </ButtonLogin>
                    {!!errMessage && <TextError>{errMessage}</TextError>}
                </ScrollView>
            </View>
        </>
    );
}
