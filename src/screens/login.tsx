import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';
import Header from '../components/Header/Header';
import HeaderBackButton from '../components/HeaderBackButton/HeaderBackButton';
import LoginForm from '../components/LoginForm/LoginForm';
import {theme} from '../style/theme';

const LoginHeader = () => (
    <Header
        centerComponent={
            <View>
                <Text style={styles.textWelcome}>Welcome Back</Text>
                <Text style={styles.textSubHeader}>
                    Please login to continue
                </Text>
            </View>
        }
    />
);

export default function Login() {
    return (
        <>
            <LoginHeader />
            <View style={styles.container}>
                <View style={styles.loginContainer}>
                    <Image
                        source={{
                            uri:
                                'https://vsudo.net/blog/wp-content/uploads/2020/08/y-tuong-thiet-ke-logo-696x688.jpg',
                        }}
                        style={styles.logo}
                        resizeMode='cover'
                    />
                    <LoginForm />
                </View>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    textWelcome: {
        fontSize: 18,
        color: 'white',
        lineHeight: 28,
        fontWeight: '700',
        textAlign: 'center',
    },
    textSubHeader: {
        fontSize: 12,
        color: 'white',
        fontWeight: '700',
        textAlign: 'center',
    },
    loginContainer: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 75,
        borderColor: theme.color.primary,
        borderWidth: 1,
        marginTop: 30,
        marginBottom: 30,
    },
});
