import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {Text, View} from 'react-native';
import Button from '../components/Button/Button';
import ConcertRegistrationButton from '../components/ConcertRegistrationButton/ConcertRegistrationButton';
import Header from '../components/Header/Header';
import HeaderBackButton from '../components/HeaderBackButton/HeaderBackButton';
import {useAuth} from '../hooks/useAuth';

export default function MyPage() {
    const {SIGNOUT, role} = useAuth();
    const navigation = useNavigation();
    return (
        <>
            <Header
                title='My Page'
                headerLeft={
                    <HeaderBackButton
                        onPress={() => navigation.navigate('my-favorite')}
                    />
                }
                headerRight={<ConcertRegistrationButton />}
            />
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Text>{`Welcome, ${role}`}</Text>
                {role === 'admin' && (
                    <Button
                        title='Register a concert'
                        onPress={() =>
                            navigation.navigate('concert-registration')
                        }
                    />
                )}
                {role !== 'guest' ? (
                    <Button
                        title='Logout'
                        onPress={() => {
                            SIGNOUT();
                            navigation.navigate('home');
                        }}
                    />
                ) : (
                    <>
                        <Text>Please log in first</Text>
                        <Button
                            title='Login'
                            onPress={() => navigation.navigate('login')}
                        />
                    </>
                )}
            </View>
        </>
    );
}
