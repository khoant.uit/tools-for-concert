import React from 'react';
import {View, ScrollView} from 'react-native';
import ResultCard from '../components/ResultCard/ResultCard';
import ConcertInfoCard from '../components/ConcertInfoCard/ConcertInfoCard';
import {
    ButtonLogin,
    TextButton,
} from '../components/LoginForm/LoginForm.styled';
import Header from '../components/Header/Header';

export default function JudgeResult() {
    return (
        <>
            <Header title='Judgement' />
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF',
                }}
            >
                <ConcertInfoCard />
                <ScrollView
                    contentContainerStyle={{
                        paddingHorizontal: 16,
                        paddingTop: 16,
                        paddingBottom: 16,
                        alignItems: 'center',
                    }}
                    style={{
                        flex: 10,
                    }}
                >
                    <ResultCard />
                    <ButtonLogin>
                        <TextButton textColor='#FFFFFF'>
                            Submit result
                        </TextButton>
                    </ButtonLogin>
                </ScrollView>
            </View>
        </>
    );
}
