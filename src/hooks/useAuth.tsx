import AsyncStorage from '@react-native-async-storage/async-storage';
import {useContext} from 'react';
import {AuthContext} from '../contexts/AuthContext';

type roleType = 'guest' | 'user' | 'admin';

const useAuth = () => {
    const [state, setState] = useContext<any>(AuthContext);

    function SIGNIN_SUCCESS({access_token, role}) {
        AsyncStorage.setItem('access_token', access_token);
        AsyncStorage.setItem('role', role);
        setState((state) => ({...state, role: role}));
    }

    function SIGNOUT() {
        AsyncStorage.removeItem('access_token');
        AsyncStorage.setItem('role', 'guest');
        setState((state) => ({...state, role: 'guest'}));
    }

    return {SIGNIN_SUCCESS, SIGNOUT, role: state.role as roleType};
};

export {useAuth};
