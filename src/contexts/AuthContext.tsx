import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {createContext, useEffect, useState} from 'react';

const AuthContext = createContext([{}, () => {}]);

// type roleType = 'guest' | 'user' | 'judge' | 'admin';

const AuthProvider = (props) => {
    const [state, setState] = useState<any>({role: 'guest'});
    useEffect(() => {
        AsyncStorage.getItem('role').then((e) => {
            setState({role: e ?? 'guest'});
            if (!e) AsyncStorage.setItem('role', 'guest');
        });
    }, []);
    return (
        <AuthContext.Provider value={[state, setState]}>
            {props.children}
        </AuthContext.Provider>
    );
};

export {AuthContext, AuthProvider};
