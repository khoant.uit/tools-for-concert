export const theme = {
    color: {
        primary: '#BFA17B',
        text: '#707070',
        lightText: '#A9A9A9',
        lightBackground: 'rgba(228, 195, 154, 0.1)',
        commentBg: 'rgba(228, 195, 154, 0.2)',
    },
};

export const ReactNativeComponentTheme = {
    colors: {
        primary: '#BFA17B',
    },
    Button: {
        titleStyle: {
            // color: 'red',
        },
        buttonStyle: {
            borderRadius: 5,
        },
    },
};
