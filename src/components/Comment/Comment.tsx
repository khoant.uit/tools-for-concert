import React from 'react';
import {Image, Text, View} from 'react-native';
import {theme} from '../../style/theme';

type props = {
    text: string;
};
export default function Comment({text}: props) {
    return (
        <View
            style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'flex-start',
            }}
        >
            <Image
                style={{width: 27, height: 27, marginRight: 5.46}}
                source={require('./../../images/avt.png')}
            />
            <View
                style={{
                    backgroundColor: theme.color.commentBg,
                    borderRadius: 5,
                    minWidth: '70%',
                    marginBottom: 10,
                    minHeight: 33,
                }}
            >
                <Text style={{color: theme.color.text}}>{text}</Text>
            </View>
        </View>
    );
}
