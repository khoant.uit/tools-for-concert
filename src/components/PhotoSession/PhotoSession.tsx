import React from 'react';
import {Image, View} from 'react-native';
import styled from 'styled-components/native';
import {AddFocus} from '../AllSvgIcons/AllSvgIcons';

const BoldText = styled.Text`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    color: ${(props) => props.theme.color.text};
`;

const PhotoContainer = styled.Pressable`
    flex: 1;
    flex-direction: row;
    justify-content: center;
`;

const Wrapper = styled.View`
    padding: 15px;
    width: 100%;
    height: 160px;
    background-color: #fff;
    margin-bottom: 10px;
    border-radius: 5px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    elevation: 8;
`;

const HeaderContent = () => {
    return (
        <View
            style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}
        >
            <BoldText>Photo</BoldText>
            <AddFocus />
        </View>
    );
};

export default function PhotoSession() {
    return (
        <Wrapper>
            <HeaderContent />
            <PhotoContainer>
                <Image
                    style={{
                        flex: 1,
                        height: undefined,
                        width: undefined,
                        resizeMode: 'contain',
                    }}
                    source={require('./../../images/bts1.png')}
                />
                <Image
                    style={{
                        flex: 1,
                        height: undefined,
                        width: undefined,
                        resizeMode: 'contain',
                    }}
                    source={require('./../../images/bts2.png')}
                />
                <Image
                    style={{
                        flex: 1,
                        height: undefined,
                        width: undefined,
                        resizeMode: 'contain',
                    }}
                    source={require('./../../images/bts3.png')}
                />
            </PhotoContainer>
        </Wrapper>
    );
}
