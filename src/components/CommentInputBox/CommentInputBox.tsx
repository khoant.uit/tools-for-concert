import React from 'react';
import Input from '../Input/Input';

export default function CommentInputBox() {
    return <Input style={{minHeight: 200, textAlignVertical: 'top'}} />;
}
