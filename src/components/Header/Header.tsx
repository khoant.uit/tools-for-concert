import React, {ReactElement, ReactNode} from 'react';
import {Header as RNEHeader} from 'react-native-elements';
import {Platform} from 'react-native';
import HeaderBackButton from '../HeaderBackButton/HeaderBackButton';

type props = {
    title?: string;
    headerLeft?: ReactNode;
    headerRight?: ReactNode;
    centerComponent?: ReactElement;
};
export default function Header({
    title,
    headerLeft,
    headerRight,
    centerComponent,
}: props) {
    return (
        <RNEHeader
            containerStyle={{
                height: Platform.OS === 'ios' ? 111 : 89.1,
            }}
            leftComponent={headerLeft ?? <HeaderBackButton />}
            leftContainerStyle={{
                display: 'flex',
                justifyContent: 'center',
            }}
            rightComponent={headerRight ?? undefined}
            rightContainerStyle={{
                display: 'flex',
                justifyContent: 'center',
            }}
            centerComponent={
                centerComponent
                    ? centerComponent
                    : {
                          text: title,
                          style: {
                              color: '#fff',
                              fontWeight: '700',
                              fontSize: 18,
                              lineHeight: 28,
                          },
                      }
            }
        />
    );
}
