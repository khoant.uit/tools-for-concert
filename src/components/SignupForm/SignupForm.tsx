import React from 'react';
import Button from '../Button/Button';
import {Input, TextTitle, Wrapper, TextNote} from './SignupForm.styled';
import {useRegisterUserMutation} from '../../graphql/gen-types';

export default function SignupForm() {
    const [registerUser] = useRegisterUserMutation({
        onCompleted: (data) => console.log(data),
        onError: (data) => console.log(data),
    });

    const signUp = () => {
        registerUser({
            variables: {
                input: {
                    email: 'khoa4@gmail.com',
                    password: 'khoa4',
                    name: 'Nguyen Tan Khoa 4',
                    companyName: 'TGL SOL',
                },
            },
        });
    };

    return (
        <Wrapper>
            <TextTitle>User name</TextTitle>
            <Input placeholder='User name' colorPlaceholder='#A9A9A9' />
            <TextTitle>Email</TextTitle>
            <Input placeholder='Email' colorPlaceholder='#A9A9A9' />
            <TextTitle>Password</TextTitle>
            <Input
                placeholder='4 to 10 characters'
                colorPlaceholder='#A9A9A9'
            />
            <TextTitle>Password (confirm)</TextTitle>
            <Input
                placeholder='Confirm your password'
                colorPlaceholder='#A9A9A9'
            />
            <TextTitle>Company (optional)</TextTitle>
            <Input placeholder='Company' colorPlaceholder='#A9A9A9' />
            <Button title='Sign up' focus size='large' onPress={signUp} />
            <TextNote>
                By click button Register, you are considered to accept our
                License
            </TextNote>
        </Wrapper>
    );
}
