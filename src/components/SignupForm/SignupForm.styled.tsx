import styled from 'styled-components/native';

const Wrapper = styled.View`
    width: 100%;
    background: #ffffff;
`;

const Input = styled.TextInput`
    background: #ffffff;
    border: 1px solid;
    border-radius: 5px;
    width: 100%;
    height: 37px;
    margin-top: 8px;
    margin-bottom: 8px;
    padding-left: 10px;
`;

const TextTitle = styled.Text`
    /* font-family: Montserrat; dude what the heck why react native why */
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 28px;
    color: #a9a9a9;
`;

const ButtonSignup = styled.TouchableOpacity`
    width: 100%;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    background-color: #e3c39a;
    margin-top: 20px;
    height: 42px;
`;

const TextButton = styled.Text`
    font-size: 20px;
    font-weight: 700;
    line-height: 28px;
    color: #ffffff;
`;

const TextNote = styled.Text`
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 28px;
    text-align: center;
    color: #a9a9a9;
    margin-top: 10px;
`;

export {Wrapper, Input, TextTitle, ButtonSignup, TextButton, TextNote};
