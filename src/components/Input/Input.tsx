import React from 'react';
import {StyleProp, TextStyle} from 'react-native';
import {Input} from 'react-native-elements';
import {theme} from '../../style/theme';

type props = {
    placeholder?: string;
    value?: string;
    onChangeText?: (e) => void;
    label?: string;
    style: StyleProp<TextStyle>;
};
export default function ({placeholder, label, onChangeText, style}: props) {
    return (
        <Input
            onChangeText={onChangeText}
            placeholder={placeholder}
            label={label}
            labelStyle={{
                fontSize: 16,
                fontWeight: 'bold',
                color: theme.color.lightText,
                marginBottom: 10,
            }}
            style={style}
            inputContainerStyle={{
                height: 40,
                backgroundColor: '#FFFFFF',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: theme.color.primary,
                paddingHorizontal: 10,
            }}
            inputStyle={{
                fontSize: 14,
            }}
            placeholderTextColor={theme.color.lightText}
        />
    );
}
