import styled from 'styled-components/native';

const TextTitle = styled.Text`
    align-self: center;
    font-size: 20px;
    font-style: normal;
    font-weight: bold;
    line-height: 22px;
    margin-top: 10px;
    color: ${(props) => props.theme.color.text};
`;

const TextContent = styled.Text`
    font-size: 14px;
    font-style: normal;
    font-weight: ${props => props.fontWeight};
    line-height: 22px;
    color: ${(props) => props.theme.color.text};
`;

const WrapperInfo = styled.View`
    flex-direction: row;
    justify-content: space-around;
    padding-left: 10px;
    padding-right: 10px;
    margin-top: 5px;
`;

export {TextTitle, WrapperInfo, TextContent};
