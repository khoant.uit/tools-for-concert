import React, {ReactNode} from 'react';
import {View, Image} from 'react-native';
import {TextTitle, WrapperInfo, TextContent} from './ConcertInfoCard.styled';
import styled from 'styled-components/native';

const Wrapper = styled.View`
    elevation: 8;
    background-color: #fff;
    border-radius: 5px;
    padding-bottom: 10px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;

type props = {
    footer?: ReactNode;
};
export default function ConcertInfoCard({footer}: props) {
    return (
        <Wrapper>
            <View style={{aspectRatio: 2.65}}>
                <Image
                    style={{
                        flex: 1,
                        resizeMode: 'contain',
                        width: undefined,
                        height: undefined,
                    }}
                    source={require('./../../images/ConcertDefault.png')}
                />
            </View>
            <TextTitle>Sing with Idols</TextTitle>
            <WrapperInfo>
                <View style={{flexDirection: 'row'}}>
                    <TextContent fontWeight='bold'>Place: </TextContent>
                    <TextContent fontWeight='100'>Osaka Stadium</TextContent>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <TextContent fontWeight='bold'>Time: </TextContent>
                    <TextContent fontWeight='100'>20h 15/03/2021</TextContent>
                </View>
            </WrapperInfo>
            <WrapperInfo>
                <View style={{flexDirection: 'row'}}>
                    <TextContent fontWeight='bold'>Category: </TextContent>
                    <TextContent fontWeight='100'>Pop Music</TextContent>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <TextContent fontWeight='bold'>Description: </TextContent>
                    <TextContent fontWeight='100'>Sing, dance</TextContent>
                </View>
            </WrapperInfo>
            {footer}
        </Wrapper>
    );
}
