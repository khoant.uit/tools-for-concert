import React from 'react';
import {View, Text} from 'react-native';
import {Container, BoldText, SubText, WrapperText} from './ResultCard.styled';
import {StarIcon, RankIcon} from '../AllSvgIcons/AllSvgIcons';
import styled from 'styled-components/native';

const Wrapper = styled.View`
    width: 100%;
    elevation: 8;
    background-color: #fff;
    border-radius: 5px;
    margin-bottom: 10px;
    padding: 15px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;

type props = {
    name?: string;
    starPoint?: string;
    rank?: string;
};

const ItemPerformance = ({name, starPoint, rank}: props) => (
    <Container>
        <SubText>{name}</SubText>
        <WrapperText>
            <SubText>{starPoint}</SubText>
        </WrapperText>
        <WrapperText>
            <SubText>{rank}</SubText>
        </WrapperText>
    </Container>
);

export default function ResultCard() {
    return (
        <Wrapper>
            <Container>
                <BoldText>List of performances</BoldText>
                <View style={{flex: 1, alignItems: 'center'}}>
                    <StarIcon />
                </View>
                <View style={{flex: 1, alignItems: 'center'}}>
                    <RankIcon />
                </View>
            </Container>
            <ItemPerformance
                name='1. Hasagi by Yasou'
                starPoint='89'
                rank='1'
            />
            <ItemPerformance
                name='2. Databeyo by Naruto'
                starPoint='85'
                rank='2'
            />
            <ItemPerformance
                name='3. Morin by Aoyama Gosho'
                starPoint='80'
                rank='3'
            />
        </Wrapper>
    );
}
