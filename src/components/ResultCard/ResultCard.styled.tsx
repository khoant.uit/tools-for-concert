import styled from 'styled-components/native';

const Container = styled.View`
    flex-direction: row;
    align-items: center;
    padding-bottom: 15px;
`;

const BoldText = styled.Text`
    flex: 4;
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    color: ${(props) => props.theme.color.text};
`;

const SubText = styled.Text`
    flex: 4;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    color: ${(props) => props.theme.color.text};
`;

const WrapperText = styled.View`
    flex: 1;
    align-items: center;
`;

export {Container, BoldText, SubText, WrapperText};
