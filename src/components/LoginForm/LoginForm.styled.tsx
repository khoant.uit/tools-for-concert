import styled from 'styled-components/native';

const Wrapper = styled.View`
    width: 100%;
    background: rgba(228, 195, 154, 0.1);
    padding-left: 10px;
    padding-right: 10px;
`;

const TextTitle = styled.Text`
    font-weight: bold;
    color: #a9a9a9;
    font-size: 16px;
    line-height: 28px;
`;

const SubText = styled.Text`
    font-size: 14px;
    line-height: 28px;
`;

const InputUser = styled.TextInput`
    background: #ffffff;
    border: ${(props) => '1px solid ' + props.theme.color.primary};
    border-radius: 5px;
    margin-top: 8px;
    height: 37px;
    padding-left: 10px;
`;

const InputPassword = styled.View`
    background: #ffffff;
    border: ${(props) => '1px solid ' + props.theme.color.primary};
    border-radius: 5px;
    margin-top: 8px;
    height: 37px;
    padding-left: 10px;
    padding-right: 10px;
    flex-direction: row;
    align-items: center;
`;

const RememberContainer = styled.View`
    flex-direction: row;
    align-items: center;
    margin-top: 8px;
    height: 30px;
`;

const ButtonLogin = styled.TouchableOpacity`
    width: 100%;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    background-color: #e3c39a;
    margin-top: 20px;
    height: 42px;
`;

const ButtonSignUp = styled.TouchableOpacity`
    width: 100%;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    margin-top: 10px;
    height: 42px;
    border: ${(props) => '1px solid ' + props.theme.color.primary};
    margin-bottom: 20px;
`;

const TextButton = styled.Text`
    font-size: 20px;
    font-weight: 700;
    line-height: 28px;
    color: ${(props) => props.textColor};
`;

const TextError = styled.Text`
    color: red;
    text-align: center;
`;
export {
    Wrapper,
    TextTitle,
    SubText,
    InputUser,
    RememberContainer,
    ButtonLogin,
    ButtonSignUp,
    TextButton,
    InputPassword,
    TextError,
};
