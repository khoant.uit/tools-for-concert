import React, {useState} from 'react';
import {TouchableOpacity, TextInput} from 'react-native';
import {
    Wrapper,
    TextTitle,
    InputUser,
    RememberContainer,
    SubText,
    InputPassword,
    TextError,
} from './LoginForm.styled';
import {CheckBox} from 'react-native-elements';
import {EyePasswordIcon} from '../AllSvgIcons/AllSvgIcons';
import {useNavigation} from '@react-navigation/native';
import {useAuth} from '../../hooks/useAuth';
import {theme} from '../../style/theme';
import Button from '../Button/Button';

export default function LoginForm() {
    const navigation = useNavigation();
    const {SIGNIN_SUCCESS} = useAuth();
    const [username, setUsername] = useState('');
    const [errMessage, setErrMessage] = useState('');
    const handleAuthenticated = () => {
        let usernamee = username.toLowerCase();
        if (usernamee === 'user')
            SIGNIN_SUCCESS({access_token: 'fake_access_token', role: 'user'});
        else if (usernamee === 'admin')
            SIGNIN_SUCCESS({access_token: 'fake_access_token', role: 'admin'});
        else {
            console.log('login fail');
            setErrMessage('Invalid username or password. Please try again.');
            return;
        }
        navigation.navigate('home', {screen: 'home'});
    };
    return (
        <Wrapper>
            <TextTitle style={{marginTop: 20}}>User name</TextTitle>
            <InputUser
                placeholder='User name'
                placeholderTextColor='#A9A9A9'
                value={username}
                onChangeText={setUsername}
            />
            <TextTitle style={{marginTop: 8}}>Password</TextTitle>
            <InputPassword>
                <TextInput
                    style={{flex: 1}}
                    placeholder='Password'
                    placeholderTextColor='#A9A9A9'
                />
                <TouchableOpacity>
                    <EyePasswordIcon />
                </TouchableOpacity>
            </InputPassword>
            <RememberContainer>
                <CheckBox
                    checked
                    checkedColor={theme.color.primary}
                    size={22}
                    uncheckedColor={theme.color.primary}
                />
                <SubText style={{color: '#A9A9A9'}}>Remember me</SubText>
            </RememberContainer>
            <Button
                title='Log in'
                size='large'
                focus
                onPress={handleAuthenticated}
            />
            {!!errMessage && <TextError>{errMessage}</TextError>}
            <TouchableOpacity
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '100%',
                    marginTop: 10,
                }}
            >
                <SubText style={{color: '#C9C9C9'}}>Can't log in?</SubText>
            </TouchableOpacity>
            <Button
                title='Sign up'
                size='large'
                titleStyle={{color: theme.color.primary}}
                onPress={() => {
                    navigation.navigate('signup');
                }}
            />
        </Wrapper>
    );
}
