import {useNavigation, useRoute} from '@react-navigation/native';
import React from 'react';
import {Pressable, View} from 'react-native';
import {BackIcon} from '../AllSvgIcons/AllSvgIcons';

type props = {
    onPress?: (e) => void;
};
export default function HeaderBackButton({onPress}: props) {
    const navigation = useNavigation();

    return (
        <Pressable
            onPress={(e) => {
                onPress ? onPress(e) : navigation.goBack();
            }}
            style={{
                paddingLeft: 10,
                paddingRight: 10,
            }}
        >
            <BackIcon />
        </Pressable>
    );
}
