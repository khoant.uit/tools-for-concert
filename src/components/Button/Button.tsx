import React from 'react';
import {StyleProp, TextStyle, ViewStyle} from 'react-native';
import {Button} from 'react-native-elements';
import {theme} from '../../style/theme';

type props = {
    title: string;
    size?: 'small' | 'medium' | 'large';
    onPress?: (e) => void;
    focus?: boolean;
    containerStyle?: StyleProp<ViewStyle>;
    titleStyle?: StyleProp<TextStyle>;
};

export default function ({
    title,
    size = 'medium',
    onPress,
    focus = false,
    containerStyle,
    titleStyle = {},
}: props) {
    const btnTitleStyle = Object.assign(
        {
            fontSize: size === 'small' ? 9 : size === 'medium' ? 11 : 18,
            color: focus ? '#fff' : theme.color.text,
            fontWeight: '700',
        },
        titleStyle,
    );

    return (
        <Button
            title={title}
            type='outline'
            onPress={onPress}
            titleStyle={btnTitleStyle}
            containerStyle={containerStyle}
            buttonStyle={{
                borderRadius: 5,
                backgroundColor: focus ? theme.color.primary : undefined,
            }}
        />
    );
}
