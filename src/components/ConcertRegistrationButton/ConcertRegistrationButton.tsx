import {useNavigation, useRoute} from '@react-navigation/core';
import React from 'react';
import {Pressable} from 'react-native';
import {useAuth} from '../../hooks/useAuth';
import {AddIcon} from '../AllSvgIcons/AllSvgIcons';

export default function HeaderAddButton() {
    const {role} = useAuth();
    const {navigate} = useNavigation();
    const route = useRoute();

    return (
        <Pressable
            style={{paddingLeft: 10, paddingRight: 10}}
            onPress={() => {
                role === 'guest'
                    ? navigate('login')
                    : role === 'admin'
                    ? navigate('concert-registration')
                    : console.log('phai tra tien');
            }}
        >
            <AddIcon />
        </Pressable>
    );
}
