import React, {useState} from 'react';
import {View, Platform, Text} from 'react-native';
import {Wrapper, Input, TextTitle, Calendar} from './ConcertForm.styled';
import {DropDownIcon} from '../AllSvgIcons/AllSvgIcons';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import {theme} from '../../style/theme';

export default function ConcertForm() {
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const onChange = (event: any, selectedDate: any) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };
    return (
        <View style={{flex: 1}}>
            <Wrapper>
                <TextTitle>Name of concert</TextTitle>
                <Input placeholder='Name' colorPlaceholder='#A9A9A9' />
                <TextTitle>Place</TextTitle>
                <Input placeholder='Place' colorPlaceholder='#A9A9A9' />
                <TextTitle>Time</TextTitle>
                <Calendar onPress={() => setShow(true)} activeOpacity={0.6}>
                    <Text
                        style={{
                            color: '#A9A9A9',
                        }}
                    >
                        {date.toDateString()}
                    </Text>
                    {show && (
                        <DateTimePicker
                            testID='dateTimePicker'
                            value={date}
                            mode='date'
                            display='calendar'
                            onChange={onChange}
                        />
                    )}
                </Calendar>
            </Wrapper>
            <Wrapper>
                <TextTitle>Category</TextTitle>
                <DropDownPicker
                    placeholder='Select a category'
                    placeholderStyle={{color: '#A9A9A9'}}
                    style={{borderColor: theme.color.primary}}
                    items={[
                        {
                            label: 'Select a category 1',
                            value: 'Select a category 1',
                        },
                        {
                            label: 'Select a category 2',
                            value: 'Select a category 2',
                        },
                        {
                            label: 'Select a category 3',
                            value: 'Select a category 3',
                        },
                    ]}
                    itemStyle={{
                        justifyContent: 'flex-start',
                    }}
                    globalTextStyle={{color: '#A9A9A9', fontWeight: 'bold'}}
                    dropDownStyle={{
                        borderColor: theme.color.primary,
                        borderWidth: 1,
                    }}
                    customArrowDown={() => <DropDownIcon />}
                    customArrowUp={() => <DropDownIcon />}
                />
                <TextTitle>Description</TextTitle>
                <Input placeholder='Description' colorPlaceholder='#A9A9A9' />
                <TextTitle>Password for a judge</TextTitle>
                <Input placeholder='Password' colorPlaceholder='#A9A9A9' />
            </Wrapper>
        </View>
    );
}
