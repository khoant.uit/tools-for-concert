import {useNavigation} from '@react-navigation/core';
import React, {useState} from 'react';
import {View, Image} from 'react-native';
import {useAuth} from '../../hooks/useAuth';
import Button from '../Button/Button';
import {EyeIcon} from '../AllSvgIcons/AllSvgIcons';
import {BoldText, SubText, ViewItem, Wrapper} from './ConcertCard.styled';

type props = {
    onPress?: any;
};

export default function ConcertCard({onPress}: props) {
    const {role} = useAuth();
    const [focus, setFocus] = useState(false);
    const navigation = useNavigation();
    return (
        <Wrapper onPress={() => navigation.navigate('concert-details')}>
            <View style={{aspectRatio: 2.65}}>
                {/* <Button title="View" onPress={onPress} /> */}
                <Image
                    style={{
                        flex: 1,
                        resizeMode: 'contain',
                        width: undefined,
                        height: undefined,
                        // marginLeft: 'auto',
                        // marginRight: 'auto',
                    }}
                    source={require('./../../images/ConcertDefault.png')}
                />
            </View>
            <View
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                }}
            >
                <ViewItem>
                    <BoldText>Sing with Idols</BoldText>
                </ViewItem>
                <ViewItem>
                    <BoldText style={{textAlign: 'center'}}>
                        20h 15/03/2021
                    </BoldText>
                </ViewItem>
                <ViewItem>
                    <BoldText style={{textAlign: 'right'}}>
                        Osaka Stadium
                    </BoldText>
                </ViewItem>
                <ViewItem
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <Image
                        style={{width: 27, height: 27}}
                        source={require('./../../images/avt.png')}
                    />
                    <View style={{marginLeft: 5}}>
                        <SubText>By YYY</SubText>
                        <SubText>16 hours ago</SubText>
                    </View>
                </ViewItem>
                <ViewItem>
                    <Button
                        title={
                            !focus ? 'Add to favorite' : 'Remove from favorite'
                        }
                        size='small'
                        onPress={() => {
                            role === 'guest'
                                ? navigation.navigate('login')
                                : setFocus(!focus);
                        }}
                        focus={focus}
                    />
                </ViewItem>
                <ViewItem
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }}
                >
                    <EyeIcon />
                    <SubText
                        style={{
                            textAlign: 'right',
                            marginLeft: 3,
                        }}
                    >
                        1555
                    </SubText>
                </ViewItem>
            </View>
        </Wrapper>
    );
}
