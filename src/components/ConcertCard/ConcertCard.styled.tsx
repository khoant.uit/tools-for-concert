import styled from 'styled-components/native';

const Wrapper = styled.Pressable`
    border-radius: 5px;
    background: #fff;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    elevation: 5;
    margin: 0 15px 11px;
`;

const BoldText = styled.Text`
    font-weight: bold;
`;

const SubText = styled.Text`
    font-size: 11px;
`;

const ViewItem = styled.View`
    flex-basis: 33.33%;
    padding: 5px;
`;
export {Wrapper, BoldText, SubText, ViewItem};
