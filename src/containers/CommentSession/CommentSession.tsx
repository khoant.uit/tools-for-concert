import React from 'react';
import Comment from '../../components/Comment/Comment';
import styled from 'styled-components/native';

const Wrapper = styled.View`
    align-self: stretch;
    background-color: #fff;
    elevation: 8;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 5px;
    padding: 15px;
`;

export default function CommentSession() {
    return (
        <Wrapper>
            <Comment text='Hasagi by Yasuo' />
            <Comment text='Hasagi by Yasuo' />
            <Comment text='Hasagi by Yasuo' />
            <Comment text='Hasagi by Yasuo' />
        </Wrapper>
    );
}
