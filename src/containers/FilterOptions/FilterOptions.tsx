import React, {useState} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import Button from '../../components/Button/Button';

type props = {
    style?: StyleProp<ViewStyle>;
};
export default function FilterOptions({style}: props) {
    const [focus, setFocus] = useState([true, false, false, false]);

    return (
        <View
            style={Object.assign(
                {
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                },
                style,
            )}
        >
            <Button
                title='By date of concert'
                focus={focus[0]}
                onPress={() => {
                    setFocus([true, false, false, false]);
                }}
                containerStyle={{flexGrow: 0, marginRight: 5}}
            />
            <Button
                title='New'
                focus={focus[1]}
                onPress={() => {
                    setFocus([false, true, false, false]);
                }}
                containerStyle={{flexGrow: 1, marginRight: 5}}
            />
            <Button
                title='View'
                focus={focus[2]}
                onPress={() => {
                    setFocus([false, false, true, false]);
                }}
                containerStyle={{flexGrow: 1, marginRight: 5}}
            />
            <Button
                title='Like'
                focus={focus[3]}
                onPress={() => {
                    setFocus([false, false, false, true]);
                }}
                containerStyle={{flexGrow: 1}}
            />
        </View>
    );
}
