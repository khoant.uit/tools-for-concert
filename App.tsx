import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/screens/home';
import ConcertRegistrationScreen from './src/screens/concert-registration';
import JudgeLoginScreen from './src/screens/judge-login';
import JudgeResultScreen from './src/screens/judge-result';
import {
    HeartIcon,
    HeartIconInvert,
    PeopleIcon,
    PeopleIconInvert,
    SearchTabBar,
    SearchTabBarInvert,
    TodayConcertTabBar,
    TodayConcertTabBarInvert,
} from './src/components/AllSvgIcons/AllSvgIcons';
import {ThemeProvider as StyledComponentThemeProvider} from 'styled-components';
import {ThemeProvider as ReactNativeThemeProvider} from 'react-native-elements';
import {ReactNativeComponentTheme, theme} from './src/style/theme';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MyPage from './src/screens/my-page';
import SignUp from './src/screens/signup';
import LoginScreen from './src/screens/login';
import {AuthProvider} from './src/contexts/AuthContext';
import ConcertDetail from './src/screens/concert-details';
import {ApolloProvider, ApolloClient, InMemoryCache} from '@apollo/client';
import TodayConcertScreen from './src/screens/today-concert';
import MyFavorite from './src/screens/my-favorite';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeTabs = () => (
    <Tab.Navigator
        tabBarOptions={{
            labelStyle: {
                fontWeight: '500',
            },
            tabStyle: {
                borderWidth: 0.5,
                borderTopWidth: 1,
                borderColor: theme.color.primary,
            },
            style: {
                borderWidth: 0.5,
                borderColor: theme.color.primary,
            },
            activeTintColor: '#fff',
            activeBackgroundColor: theme.color.primary,
        }}
    >
        <Tab.Screen
            name={'home'}
            component={HomeScreen}
            options={{
                tabBarIcon: ({focused}) =>
                    !focused ? <SearchTabBar /> : <SearchTabBarInvert />,
                title: 'Home',
            }}
        />
        <Tab.Screen
            name='today-concert'
            component={TodayConcertScreen}
            options={{
                tabBarIcon: ({focused}) =>
                    !focused ? (
                        <TodayConcertTabBar />
                    ) : (
                        <TodayConcertTabBarInvert />
                    ),
                title: "Today's Concert",
            }}
        />
        <Tab.Screen
            name='my-favorite'
            component={MyFavorite}
            options={{
                tabBarIcon: ({focused}) =>
                    !focused ? <HeartIcon /> : <HeartIconInvert />,
                title: 'My Favorite',
            }}
        />
        <Tab.Screen
            name='my-page'
            component={MyPage}
            options={{
                tabBarIcon: ({focused}) =>
                    !focused ? <PeopleIcon /> : <PeopleIconInvert />,
                title: 'My Page',
            }}
        />
    </Tab.Navigator>
);

const ToolsForConcertApp = () => {
    const client = new ApolloClient({
        uri:
            'https://us-central1-tool-concert-stag.cloudfunctions.net/api/graphql/',
        cache: new InMemoryCache(),
    });

    return (
        <ApolloProvider client={client}>
            <AuthProvider>
                <ReactNativeThemeProvider theme={ReactNativeComponentTheme}>
                    <StyledComponentThemeProvider theme={theme}>
                        {/* must use for react-native-element */}
                        <SafeAreaProvider>
                            <NavigationContainer>
                                <Stack.Navigator
                                    screenOptions={{headerShown: false}}
                                >
                                    <Stack.Screen
                                        name='home'
                                        component={HomeTabs}
                                    />
                                    <Stack.Screen
                                        name='login'
                                        component={LoginScreen}
                                    />
                                    <Stack.Screen
                                        name='signup'
                                        component={SignUp}
                                    />
                                    <Stack.Screen
                                        name={'judge-login'}
                                        component={JudgeLoginScreen}
                                    />
                                    <Stack.Screen
                                        name={'judge-result'}
                                        component={JudgeResultScreen}
                                    />
                                    <Stack.Screen
                                        name={'concert-registration'}
                                        component={ConcertRegistrationScreen}
                                    />
                                    <Stack.Screen
                                        name={'concert-details'}
                                        component={ConcertDetail}
                                    />
                                </Stack.Navigator>
                            </NavigationContainer>
                        </SafeAreaProvider>
                    </StyledComponentThemeProvider>
                </ReactNativeThemeProvider>
            </AuthProvider>
        </ApolloProvider>
    );
};

export default ToolsForConcertApp;
